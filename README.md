# Suivi d'Activités Sportives


# Protocole d'Utilisation de l'Application "Suivi d'Activités Sportives"

## 1. Introduction
L'application **"Suivi d'Activités Sportives"** est conçue pour aider à enregistrer et suivre vos séances d'activités physiques. Ce guide vous expliquera comment utiliser l'application efficacement.

## 2. Création d'un Compte
- Au lancement, vous devez fournir des informations de base (âge, poids, taille, sexe) pour l'estimation des calories brûlées.

## 3. Enregistrement d'une Séance d'Activité
- Après création de votre compte, enregistrez une séance en cliquant sur le bouton dédié.
- Sélectionnez la **date** (JJ/MM/AAAA), **type d'activité** via le menu déroulant, et **durée** en minutes.
- Utilisez le bouton **"Estimer Calories"** pour voir les calories brûlées.

## 4. Suivi Hebdomadaire, Mensuel et Annuel
- Les séances sont organisées pour offrir un suivi hebdomadaire, mensuel, et annuel.
- Consultez ces suivis dans l'interface principale pour voir vos progrès.

En suivant ces étapes, vous maximiserez les bénéfices de l'application "Suivi d'Activités Sportives" pour atteindre vos objectifs de fitness et de bien-être.