import tkinter as tk
from tkinter import ttk
from datetime import datetime, timedelta

class ActiviteSportive:
    def __init__(self, type_activite, date, duree, distance, calories_brulees):
        """
        Initialise un objet ActiviteSportive avec les attributs spécifiés.

        Args:
            type_activite (str): Le type d'activité sportive.
            date (str): La date de l'activité au format "jour/mois/année".
            duree (int): La durée de l'activité en minutes.
            distance (float): La distance parcourue pendant l'activité en kilomètres.
            calories_brulees (float): Le nombre de calories brûlées pendant l'activité.
        """
        self.type_activite = type_activite
        self.date = date
        self.duree = duree
        self.distance = distance
        self.calories_brulees = calories_brulees

class SuiviSportifApp:
    def __init__(self, master):
        """
        Initialise l'application de suivi d'activités sportives.

        Args:
            master : Tkinter.Tk
                La fenêtre principale de l'application.

        Returns:
            None
        """
        self.master = master
        master.title("Suivi d'Activités Sportives")

        # Titre
        self.label = tk.Label(master, text="Bienvenue dans l'application de suivi d'activités sportives !", font=("Helvetica", 18, "bold"), bg="#009688", fg="white")
        self.label.pack(pady=(30, 10))

        # Cadre principal
        self.frame = tk.Frame(master, bg="white", padx=20, pady=10)
        self.frame.pack(pady=20, padx=20)

        # Entrées et étiquettes
        labels_texts = ["Date :", "Votre âge :", "Votre poids (en kg) :", "Votre taille (en cm) :", "Votre sexe biologique :", "Choisissez votre activité :", "Durée (minutes) :", "Calories brûlées (estimation) :"]
        self.entries = []
        for i, text in enumerate(labels_texts):
            label = tk.Label(self.frame, text=text, bg="white", font=("Helvetica", 12, "bold"))
            label.grid(row=i, column=0, sticky="w", pady=(10, 5))
            entry = tk.Entry(self.frame, font=("Helvetica", 12))
            entry.grid(row=i, column=1, pady=(10, 5))
            self.entries.append(entry)

        # Rendre la dernière entrée non modifiable
        self.entries[-1].config(state='readonly')

        # Menu déroulant pour le sexe biologique
        self.sexes = ["Homme", "Femme", "Ne se prononce pas"]
        self.sexe_selectionne = tk.StringVar(master)
        self.sexe_selectionne.set(self.sexes[0])
        self.sexe_menu = ttk.Combobox(self.frame, textvariable=self.sexe_selectionne, values=self.sexes, state="readonly", font=("Helvetica", 12))
        self.sexe_menu.grid(row=4, column=1, pady=(10, 5))

        # Menu déroulant pour l'activité
        self.activites = ["Marche", "Course", "Vélo", "Natation", "Saut à la corde", "Randonnée", "Yoga", "Football", "Tennis", "Gymnastique"]
        self.activite_selectionnee = tk.StringVar(master)
        self.activite_selectionnee.set(self.activites[0])
        self.activite_menu = ttk.Combobox(self.frame, textvariable=self.activite_selectionnee, values=self.activites, state="readonly", font=("Helvetica", 12))
        self.activite_menu.grid(row=5, column=1, pady=(10, 5))

        # Bouton pour estimer les calories
        self.submit_button = tk.Button(self.frame, text="Estimer Calories", command=self.estimer_calories, bg="#4CAF50", fg="white", font=("Helvetica", 12, "bold"), relief=tk.GROOVE)
        self.submit_button.grid(row=8, columnspan=2, pady=20)

        # Étiquette pour le résultat
        self.resultat_label = tk.Label(master, text="", bg="#009688", fg="white", font=("Helvetica", 14, "italic"))
        self.resultat_label.pack(pady=(10, 30))

        # Initialisation du suivi hebdomadaire, mensuel et annuel
        self.seances = []

        # Liste déroulante pour le suivi quotidien
        self.calories_par_jour_label = tk.Label(master, text="Sélectionnez une date pour voir le nombre de calories brûlées ce jour-là:", font=("Helvetica", 12, "bold"), bg="white")
        self.calories_par_jour_label.pack(pady=(10, 0))
        self.date_selectionnee = tk.StringVar(master)
        self.date_selectionnee.trace("w", self.afficher_calories_par_jour)
        self.date_menu = ttk.Combobox(master, textvariable=self.date_selectionnee, state="readonly", font=("Helvetica", 12))
        self.date_menu.pack()

    def estimer_calories(self):
        """
        Estime le nombre de calories brûlées pendant une activité sportive en utilisant les valeurs entrées par l'utilisateur.

        Args:
            Aucun

        Returns:
            None
        """
        # Récupération des valeurs entrées par l'utilisateur
        date = self.entries[0].get()
        date_obj = datetime.strptime(date, "%d/%m/%Y")
        age = int(self.entries[1].get())
        poids = float(self.entries[2].get())
        taille = float(self.entries[3].get())
        sexe = self.sexe_selectionne.get()
        duree = int(self.entries[6].get())

        activite_selectionnee = self.activite_selectionnee.get()
        calories_brulees = self.calculer_calories(age, poids, taille, duree, sexe, activite_selectionnee)

        # Ajouter la séance à la liste
        seance = ActiviteSportive(activite_selectionnee, date_obj, duree, 0, calories_brulees)
        self.seances.append(seance)

        # Afficher le résultat
        self.entries[-1].config(state='normal')  # Rendre la dernière entrée modifiable temporairement
        self.entries[-1].delete(0, tk.END)
        self.entries[-1].insert(0, f"{calories_brulees:.2f} calories")
        self.entries[-1].config(state='readonly')  # Rendre la dernière entrée non modifiable à nouveau
        self.resultat_label.config(text=f"Vous avez brûlé environ {calories_brulees:.2f} calories lors de votre activité de {activite_selectionnee}.")

        # Mettre à jour le suivi
        self.mettre_a_jour_suivi()

        # Mettre à jour la liste déroulante des dates
        dates = sorted(set(seance.date.strftime("%d/%m/%Y") for seance in self.seances))
        self.date_menu['values'] = dates

    def calculer_calories(self, age, poids, taille, duree, sexe, activite_selectionnee):
        """
        Calcule le nombre de calories brûlées pendant une activité sportive.

        Args:
            age (int): L'âge de la personne en années.
            poids (float): Le poids de la personne en kilogrammes.
            taille (float): La taille de la personne en centimètres.
            duree (int): La durée de l'activité en minutes.
            sexe (str): Le sexe biologique de la personne (doit être 'Homme', 'Femme' ou 'Ne se prononce pas').
            activite_selectionnee (str): Le type d'activité sportive sélectionnée.

        Returns:
            float: Le nombre de calories brûlées pendant l'activité.
        """
        # Coefficients pour le calcul des calories en fonction de l'activité
        coefficients = {
            "Marche": 4.0,
            "Course": 10.0,
            "Vélo": 7.0,
            "Natation": 9.0,
            "Saut à la corde": 12.0,
            "Randonnée": 5.0,
            "Yoga": 3.0,
            "Football": 11.0,
            "Tennis": 8.0,
            "Gymnastique": 6.0
        }

        # Coefficient pour le sexe biologique
        coefficient_sexe = 1.0
        if sexe == "Femme":
            coefficient_sexe = 0.9

        # Calcul des calories brûlées en utilisant une formule spécifique à chaque activité et en tenant compte du sexe
        coefficient = coefficients.get(activite_selectionnee, 1.0)
        calories_brulees = coefficient * poids * duree / 60 * coefficient_sexe  # Calcul en fonction du coefficient, de la durée (en heures) et du sexe

        return calories_brulees

    def mettre_a_jour_suivi(self):
        """
        Met à jour le suivi des séances d'activités pour la semaine en cours, le mois en cours et l'année en cours.

        Args:
            Aucun

        Returns:
            None
        """
        # Obtenir la date d'aujourd'hui
        date_today = datetime.now().date()

        # Obtenir les dates de début et de fin de la semaine en cours
        date_debut_semaine = date_today - timedelta(days=date_today.weekday())
        date_fin_semaine = date_debut_semaine + timedelta(days=6)

        # Filtrer les séances de la semaine en cours
        seances_semaine = [seance for seance in self.seances if date_debut_semaine <= seance.date.date() <= date_fin_semaine]

        # Calculer les calories brûlées cette semaine
        calories_semaine = sum(seance.calories_brulees for seance in seances_semaine)

        # Initialiser le suivi par mois
        suivi_mois = {}

        # Initialiser le suivi annuel
        suivi_annuel = 0

        # Obtenir l'année en cours
        annee_en_cours = datetime.now().year

        # Parcourir toutes les séances et mettre à jour le suivi par mois et annuel
        for seance in self.seances:
            mois_seance = seance.date.replace(day=1)
            if mois_seance.year == annee_en_cours:  # Assurer que la séance est de l'année en cours
                if mois_seance not in suivi_mois:
                    suivi_mois[mois_seance] = 0
                suivi_mois[mois_seance] += seance.calories_brulees
                suivi_annuel += seance.calories_brulees

        # Construction du texte de suivi pour chaque mois
        suivi_text = ""
        for mois, calories in suivi_mois.items():
            suivi_text += f"Suivi du mois ({mois.strftime('%B %Y')}):\nCalories brûlées: {calories:.2f}\n\n"

        # Ajouter le suivi annuel au texte
        suivi_text += f"Suivi de l'année ({annee_en_cours}):\nCalories brûlées: {suivi_annuel:.2f}\n"

        # Afficher le suivi
        self.resultat_label.config(text=suivi_text)

    def afficher_calories_par_jour(self, *args):
        """
        Affiche le nombre de calories brûlées pour une journée sélectionnée.

        Args:
            args: Les arguments passés à la fonction (peut être vide).

        Returns:
            None
        """
        selected_date = self.date_selectionnee.get()
        seances_jour = [seance for seance in self.seances if seance.date.strftime("%d/%m/%Y") == selected_date]
        calories_jour = sum(seance.calories_brulees for seance in seances_jour)
        self.resultat_label.config(text=f"Vous avez brûlé environ {calories_jour:.2f} calories le {selected_date}.")

# Lancement de l'application
if __name__ == "__main__":
    root = tk.Tk()
    app = SuiviSportifApp(root)
    root.resizable(False, False)
    root.mainloop()
